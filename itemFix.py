# from operator import truediv
from json import load
from utils import *

def loadFIx() -> dict:
    with open(localesFilePath + LANGUAGE_DICT['lang "Simplified Chinese"']+"/itemFix.json", 'r', encoding='utf8') as load_f:
        return json.load(load_f)

FIX = loadFIx()

def genDuplateName(refName:str) -> str:
    names = FIX['duplicate'].get(refName)
    if names == None:
        return
    for name in names:
        yield name

