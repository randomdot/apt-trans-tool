from tradeStats import createTradeStatsCacheFile
from descriptions import createDescriptionCacheFile
from tradeItems import createTradeItemsCacheFile
# run needed only once when ggpk description files changed
def createCacheFiles(langTag:str):
    createDescriptionCacheFile(langTag)
    createTradeStatsCacheFile(langTag)
    createTradeItemsCacheFile(langTag)


# all suported language in ggpk description files
# --------------------------------------------------------- #
#         language tag            | folder name  |  index   #
# --------------------------------------------------------- #
#   'lang "Traditional Chinese"'  |   "zh_TW"    |    0     #
#   'lang "German"'               |   "de"       |    1     #
#   'lang "Russian"'              |   "ru"       |    2     #
#   'lang "Spanish"'              |   "es"       |    3     #
#   'lang "Portuguese"'           |   "pt"       |    4     #
#   'lang "Korean"':              |   "ko"       |    5     #
#   'lang "French"':              |   "fr"       |    6     #
#   'lang "Thai"'                 |   "th"       |    7     #
#   'lang "Simplified Chinese"'   |   "zh_CN"    |    8     #
#   'lang "Japanese"'             |   "ja"       |    9     #
# --------------------------------------------------------- #
createCacheFiles('lang "Simplified Chinese"')