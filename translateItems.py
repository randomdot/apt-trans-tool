# from copyreg import constructor
# import string
from itemFix import genDuplateName, FIX
from tradeItems import loadTemplateItems, mergeItems
from utils import *

def createItemsFile(filePathName:str):
    template = loadTemplateItems()
    transDict = mergeItems()
    count = 0
    gen = None
    for item in template:
        refName = item['refName']
        if refName in FIX['duplicate'].keys():
            if gen == None:
                gen = genDuplateName(refName)
            try:
                item["name"] = next(gen)
                count += 1
                continue
            except(StopIteration):
                gen = None
        else:
            gen = None

        text = transDict.get(refName)
        if text:
            item["name"] = text
            count += 1

    ndJsonString = jsonToNdJson(template)
    with open("./locales/zh_CN/Items.ndjson", "w", encoding='utf8') as load_f:
        load_f.write(ndJsonString)
        print("./locales/zh_CN/items.ndjson, 找到", count, "条","未找到", len(template)-count,"条")


createItemsFile("./locales/zh_CN/itemsDict.ndjson")


