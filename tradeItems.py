import re
import json
from utils import *

# load translation dict from ggpk file
def loadCsv(filePathName:str):
    with open(filePathName, 'r', encoding='utf8') as load_f:
        return load_f.read().splitlines()


def elem(line:str):
    ref = re.match(r'^Metadata.*?(?=,)',line)
    name = re.search(r'(?<=",\d,\d,").*(?=",Metadata)', line)
    if name == None:
        name = re.search(r'(?<=",\d,\d,).*(?=,Metadata)', line)
    return (ref.group(), name.group())


def itemsWithLines(lines:list):
    lines.pop(0)            # get rid of First line because it's usless
    if lines[-1] == "" : lines.pop()
    return dict(elem(line) for line in lines)


def loadGgpkItems():
    enDict = itemsWithLines(loadCsv("./Res/PoeItemData/en/BaseItemTypes.csv"))
    cnDict = itemsWithLines(loadCsv("./Res/PoeItemData/cn/BaseItemTypes.csv"))
    return {enDict[key]:cnDict[key] for key in cnDict if enDict.get(key)}


# load translation dict from poedb trade api file
def loadPoedbTradeItems() -> dict:
    with open("./Res/locales/zh_CN/trade.json", 'r', encoding='utf8') as load_f:
        items = json.load(load_f)
        return  {item["us"]:item["lang"] for item in items}

# 
def loadTranslatedItems() -> dict:
        with open("./Res/locales/zh_CN/items.ndjson", 'r', encoding='utf8') as load_f:
            items = ndJsonToJson(load_f.read())
            return {item["refName"]:item["name"] for item in items}


def loadTemplateItems() -> dict:
        with open("./Res/items.ndjson", 'r', encoding='utf8') as load_f:
            return ndJsonToJson(load_f.read())


# merge all translation dicts to one dict
def mergeItems():
    ggpkDict = loadGgpkItems()
    poeDbDict = loadPoedbTradeItems()
    translatedDict = loadTranslatedItems()
    return translatedDict | poeDbDict | ggpkDict


def createTradeItemsCacheFile(langTag:str):
    items = mergeItems()
    fileNamePath = localesFilePath + LANGUAGE_DICT[langTag] + "/itemsCache.json"
    with open(fileNamePath, 'w', encoding='utf8') as save_f:
        save_f.write(json.dumps(items, ensure_ascii=False, separators=(',\n', ':')))
        print('File saved:', fileNamePath)


if __name__ == '__main__':
    createTradeItemsCacheFile('lang "Simplified Chinese"')