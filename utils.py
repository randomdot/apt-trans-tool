import json
import chardet

# For test, set: TEST_MODE = True
TEST_MODE = False

# content.ggpk description file list
STATE_DESCRIPTION_FILE_LIST = ["active_skill_gem_stat_descriptions.txt",
                            "advanced_mod_stat_descriptions.txt",
                            "atlas_stat_descriptions.txt",
                            "aura_skill_stat_descriptions.txt",
                            "banner_aura_skill_stat_descriptions.txt",
                            "beam_skill_stat_descriptions.txt",
                            "brand_skill_stat_descriptions.txt",
                            "buff_skill_stat_descriptions.txt",
                            "chest_stat_descriptions.txt",
                            "curse_skill_stat_descriptions.txt",
                            "debuff_skill_stat_descriptions.txt",
                            "expedition_relic_stat_descriptions.txt",
                            "gem_stat_descriptions.txt",
                            "heist_equipment_stat_descriptions.txt",
                            "leaguestone_stat_descriptions.txt",
                            "map_stat_descriptions.txt",
                            "minion_attack_skill_stat_descriptions.txt",
                            "minion_skill_stat_descriptions.txt",
                            "minion_spell_damage_skill_stat_descriptions.txt",
                            "minion_spell_skill_stat_descriptions.txt",
                            "monster_stat_descriptions.txt",
                            "offering_skill_stat_descriptions.txt",
                            "passive_skill_aura_stat_descriptions.txt",
                            "passive_skill_stat_descriptions.txt",
                            "primordial_altar_stat_descriptions.txt",
                            "secondary_debuff_skill_stat_descriptions.txt",
                            "sentinel_stat_descriptions.txt",
                            "single_minion_spell_skill_stat_descriptions.txt",
                            "skill_stat_descriptions.txt",
                            "stat_descriptions.txt",
                            "variable_duration_skill_stat_descriptions.txt"]

LANGUAGE_DICT = {'lang "Traditional Chinese"': "zh_TW",
            'lang "German"': "de",
            'lang "Russian"': "ru",
            'lang "Spanish"': "es",
            'lang "Portuguese"': "pt",
            'lang "Korean"': "ko",
            'lang "French"': "fr",
            'lang "Thai"': "th",
            'lang "Simplified Chinese"': "zh_CN",
            'lang "Japanese"': "ja"}


if TEST_MODE:
    descriptionFileList = ["stat_descriptions.txt"]
    baseFilePath = './Res/testRes/'
    localesFilePath = "./Res/testRes/locales/"
    statFileOutputName = 'statsTest.ndjson'
    descriptionCacheName = 'descriptionCacheTest.json'
else:
    descriptionFileList = STATE_DESCRIPTION_FILE_LIST
    baseFilePath = './Res/'
    localesFilePath = './Res/locales/'
    statFileOutputName = 'stats.ndjson'
    descriptionCacheName = 'descriptionCache.json'


def getFileEncode(filename):
    with open(filename, 'rb') as load_f:
        return chardet.detect(load_f.read())["encoding"]


def ndJsonToJson(ndJson=""):
    ndJson = ndJson.splitlines()
    if len(ndJson) > 0: 
        if ndJson[-1] == '':
            ndJson.pop()
        return [json.loads(i) for i in ndJson]


def jsonToNdJson(jsonData):
    ndJson = ''
    for item in jsonData:
        ndJson += json.dumps(item, ensure_ascii=False,separators=(',', ':')) + '\n'
    return ndJson
