# AptTransTool

#### 介绍
将Awakened POE trade中的stats.ndjson及items.ndjson翻译成国服简体中文

#### 软件架构
python


#### 安装教程

1.  运行translateStats.py 生成 ./locales/zh_CN/stats.ndjson
2.  运行translateItems.py 生成 ./locales/zh_CN/items.ndjson


#### 使用说明

1.  将items.ndjson与stats.ndjson复制到awakened poe trade代码:renderer/public/data/zh_CN/中,由于apt未实现对简体中文件的支持,可以暂时将这两个文件复制到data下的cmn-hant中覆盖繁体中文,编译后语言设置成繁体中文即可显示简体中文.

#### 资源文件
1.  Res/locales/zh_CN/StatDescriptions/*
    来自poe content.ggpk 文件，包括了大部分游戏内的词缀。

2.  Res/items.json
    来自 poe.game.qq.com/api/trade/data/items.json，包括了所有中文物品名称，但因为没有英文对照，暂未使用

3.  Res/items.ndjson
    来自 nga.cn 的 jdchjhh 提供的物品中英文对照翻译

4.  Res/stats.json
    来自 poe.game.qq.com/api/data/stats.json，包括了所有中文词缀

5.  Res/trade.json
    来自poedb.tw，包括了部分中英文对照翻译

6.  Res/tradeStatsCache.json
    临时文件

7.  Res/PoeItemData/*
    来自 poe content.ggpk 文件，包括了一部分当前游戏中最新的物品中英文对照，包括新赛季物品

8.  Res/testRes/*
    用于测试的文件

9.  Res/items.ndjson
10. Res/stats.ndjson
    来自Awakened PoE Trade，为翻译目标文件
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
