# from asyncio.windows_events import NULL
import os
import re
import json
from utils import *
from tradeStats import loadTradeStats
from descriptions import createDescriptionCacheFile, createAllDescriptionCacheFiles

# Load stats.ndjson file for translating
def statTemplate() -> list:
    with open(baseFilePath+"stats.ndjson", 'r', encoding='utf8') as load_f:
        return ndJsonToJson(load_f.read())

def findInDescription(refName:str, descriptions:dict) -> str:
    withPlus = re.sub("(?<!\+|-)(?<!-\s)#(?!-|\s-|\s-)", "+#", refName)
    name = descriptions.get(refName)
    return name if name else descriptions.get(withPlus)

def findInTradeStats(ids:str, tradeStats:dict) -> str:
    modTypes = ids.keys()
    modIds = [modId for modType in modTypes for modId in ids[modType]]
    for modId in modIds:
        result = tradeStats.get(modId)
        if result: return result

# translate stats
def translateStat(statTemplate: dict, descriptions: dict, langTag: str) -> dict:
    # statKeys = descriptions.keys()
    count = nCount = 0
    tradeStats = loadTradeStats(langTag)

    print("Description file ready, translating...")
    for item in statTemplate:
        matchers = item["matchers"]
        ids = item["trade"]['ids']
        for mod in matchers:
            templateStat = mod["string"]
            localizedStat = findInDescription(templateStat, descriptions)
            if localizedStat == None:
                localizedStat = findInTradeStats(ids, tradeStats)

            if localizedStat:
                mod["string"]= localizedStat
                count += 1
            else:
                nCount += 1

    print("translate", count, "stats, and ",
          nCount, "not found! traslating completed")
    return statTemplate


# load translate dictionary created from ggpk description file if exist, or create it
def loadDescriptions(langTag) -> list:
    loadPath = localesFilePath + LANGUAGE_DICT[langTag] + "/" + descriptionCacheName
    if not os.path.exists(loadPath):
        createDescriptionCacheFile(langTag)
        print("language translation dict created: " + loadPath)

    with open(loadPath, 'r', encoding='utf8') as load_f:
        return json.load(load_f)


# save translated stats file to stats.ndjson
def createLocalizedStatsFile(langTag: str):
    print("Translating: " + LANGUAGE_DICT[langTag])
    stats = statTemplate()
    transDict = loadDescriptions(langTag)
    stats = translateStat(stats, transDict, langTag)

    saveFolder = "./locales/" + LANGUAGE_DICT[langTag] + '/'
    if not os.path.exists(saveFolder):
        os.makedirs(saveFolder)
    with open(saveFolder+statFileOutputName, "w", encoding='utf8') as load_f:
        load_f.write(jsonToNdJson(stats))
        print("File saved: "+saveFolder)

# Translate all language in ggpk description and save them in their own locale folder
def createAllLocalizedStatsFile():
    langs = LANGUAGE_DICT.keys()
    for langTag in langs:
        createLocalizedStatsFile(langTag)

# all suported language in ggpk description files
# --------------------------------------------------------- #
#         language tag            | folder name  |  index   #
# --------------------------------------------------------- #
#   'lang "Traditional Chinese"'  |   "zh_TW"    |    0     #
#   'lang "German"'               |   "de"       |    1     #
#   'lang "Russian"'              |   "ru"       |    2     #
#   'lang "Spanish"'              |   "es"       |    3     #
#   'lang "Portuguese"'           |   "pt"       |    4     #
#   'lang "Korean"':              |   "ko"       |    5     #
#   'lang "French"':              |   "fr"       |    6     #
#   'lang "Thai"'                 |   "th"       |    7     #
#   'lang "Simplified Chinese"'   |   "zh_CN"    |    8     #
#   'lang "Japanese"'             |   "ja"       |    9     #
# --------------------------------------------------------- #

# -----------------------------------------
# |    method one: create by key index    |
# -----------------------------------------

#  createLocalizedStatsFile(list(langDict.keys())[2])

# --------------------------------------------
# |    method two: create by language tag    |
# --------------------------------------------

createLocalizedStatsFile('lang "Simplified Chinese"')

# ----------------------------------------------------
# |   method three: create all language in langDict  |
# ----------------------------------------------------

#  createAllLocalizedStatsFile()
