
# from fileinput import filename
from utils import LANGUAGE_DICT, STATE_DESCRIPTION_FILE_LIST, getFileEncode, descriptionFileList, localesFilePath, descriptionCacheName, TEST_MODE
import re
import json

# stats file proccessing
def findLocalizedName(aString) -> str:
    pat = "{\d?:?\+?d?.}"
    aString = re.sub(pat, "#", aString)
    startIndex = aString.find('"')
    endIndex = aString.rfind('"')
    return aString[startIndex+1:endIndex]

def getRefName(aString) -> str:
    pat = "{\d?:?\+?d?.}"   # Replace {0} {1} {0:d} {0:+d} ... to '#'
    pat2 = "(?<!#)-#"       # Get rid of '-'
    aString = re.sub(pat2, "#", re.sub(pat, "#", aString))
    startIndex = aString.find('"')
    endIndex = aString.rfind('"')
    return aString[startIndex+1:endIndex]

def parseSection(lines: list, langTag: str) -> dict:
    count = int(lines[1]) # ref(english) string line count
    if count > 0:
        refLines = [getRefName(item) for item in lines[2:count + 2]]  # store ref string in templateLines list

    if langTag in lines:   # Find language to translate by language tag
        index = lines.index(langTag)
        count = int(lines[index+1]) # Get language line count, shoud equal to ref line count
        if count > 0:
            localizedLines = [findLocalizedName(item)
                         for item in lines[index+2:index+2+count]]
    else:
        print("warning, Can't find " + langTag + "of",
              refLines, "at section:", lines[0])
        return None

    return dict(zip(refLines, localizedLines))

# create en to zh_CN dictionary by using stats_descriptions.txt file in conten.ggpk of path of exile
# It will be used to translate en to cn language in stats.ndjson file
def parseDescription(filename: str, langTag: str) -> dict:
    section = []
    descriptions = {}
    filePathName = localesFilePath + LANGUAGE_DICT[langTag] + "/StatDescriptions/" + filename

    print("proccessing description files...")
    print(filename + "...")
    fileEncoding = getFileEncode(filePathName)
    with open(filePathName, 'r', encoding=fileEncoding) as load_f:
        lines = load_f.read().splitlines()

    # Find description identify first
    for line in lines:
        if (line[:11] == "description"):
            lines = lines[lines.index(line):-1]
            break

    for line in lines:
        line = line.strip().replace("\\n", "\n")
        if line.startswith("description") or line.startswith("no_description"):
            if len(section) > 0:
                newDict = parseSection(section, langTag)
                if newDict:
                    descriptions.update(newDict)
            section = []
            continue
        else:
            if len(line):
                section.append(line)  # 只添加非空行
    else:
        if len(section):
            descriptions.update(parseSection(section, langTag))

    return descriptions


def mergeDescriptions(langTag: str) -> dict:
    descriptions: dict = {}
    for filename in descriptionFileList:
        newDict = parseDescription(filename, langTag)
        descriptions.update(newDict)
    # add missing cluster stat
    with open(localesFilePath + LANGUAGE_DICT[langTag] +"/statsCluster.json", "r", encoding='utf8') as load_f:
        descriptions.update(json.load(load_f))

    # fix some stat translation error
    with open(localesFilePath + LANGUAGE_DICT[langTag] +"/descriptionFix.json", "r", encoding='utf8') as load_f:
        fix = json.load(load_f)
        for k,v in fix.items():
            descriptions[k] = v
    return descriptions


def createDescriptionCacheFile(langTag: str):
    descriptions = mergeDescriptions(langTag)
    filePathName = localesFilePath + LANGUAGE_DICT[langTag] + "/" + descriptionCacheName
    jsonString = json.dumps(descriptions, ensure_ascii=False,
                            indent=4, separators=(',', ': '))
    with open(filePathName, 'w', encoding='utf8') as save_f:
        save_f.write(jsonString)
        print("Description cache saved: "+ filePathName)


def createAllDescriptionCacheFiles():
    for langTag in LANGUAGE_DICT:
        createDescriptionCacheFile(langTag)

if __name__ == '__main__':
    createDescriptionCacheFile('lang "Simplified Chinese"')
