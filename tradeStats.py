import json
# from msilib.schema import File
from utils import *

#  stats.json from poe.game.qq.com/api/trade/data/stats
#  File struct example:
#  {"result":[{"label":"综合","entries":[{"id":"pseudo.pseudo_total_cold_resistance","text":"+#%最大冰霜抗性","type":"pseudo"},...]},...]}

def createTradeStatsCacheFile(langTag:str):
    lang = LANGUAGE_DICT[langTag]
    with open(localesFilePath + lang + '/stats.json', 'r', encoding='utf8') as load_f:
        statsDict:dict = json.load(load_f)
        statsList:list = {entries['id']:entries['text'] for categ in statsDict["result"] for entries in categ['entries']}
        jsonString = json.dumps(statsList, ensure_ascii=False, separators=(',\n', ': '))

    with open(localesFilePath + lang + '/tradeStatsCache.json', 'w', encoding='utf8') as save_f:
        save_f.write(jsonString)
        print("Trade stats cache file saved:", localesFilePath+lang+'/tradeStatsCache.json')

def loadTradeStats(langTag:str) -> dict:
    with open(localesFilePath + LANGUAGE_DICT[langTag] + '/tradeStatsCache.json', 'r', encoding='utf8') as load_f:
        return json.load(load_f)

if __name__ == '__main__':
    createTradeStatsCacheFile('lang "Simplified Chinese"')
