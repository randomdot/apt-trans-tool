def fun1(fun):
    def fun2():
        print("你好")
        fun()
        print("完成")

    return fun2

# fun = fun1()
# fun(": arg")
@fun1
def demo():
    print("demo")
""" 
demo() """
